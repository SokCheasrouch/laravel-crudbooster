<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cateogories extends Model
{
    protected $table = 'cateogories';

    public function products() {
        return $this->hasMany("Product");
    }
}
