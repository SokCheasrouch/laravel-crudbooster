<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends Controller
{

    public $auth;

    function __construct() {
        error_log("Hello from");
        $factory = (new Factory)->withServiceAccount(__DIR__.'/FirebaseKey.json');

        $this->auth = $factory->createAuth();
    }

    public function index() {
        try {
            // $user = $auth->getUser('some-uid');
            $user = $this->auth->getUserByEmail('user@examp3le.com');
            $users = $this->auth->listUsers($defaultMaxResults = 1000, $defaultBatchSize = 1000);
            // $user = $auth->getUserByPhoneNumber('+49-123-456789');
            // error_log($user.uid);
            $a = 0;
            $usera = array();
            foreach ($users as $user) {
                /** @var \Kreait\Firebase\Auth\UserRecord $user */
                // ...
                $a++;
                $usera[] = $user;
            }
            return (['message'=> $usera]);
        } catch (\Kreait\Firebase\Exception\Auth\UserNotFound $e) {
            error_log('Some message here. error'.$e->getMessage());
            echo $e->getMessage();
        }
        return (['message'=> "hello"]);
    }

    public function createUserByEmailAndPassword($email, $password) {
        $userProperties = [
            'email' => $email,
            'password' => $password
        ];

        try {
            $createdUser = $this->auth->createUser($userProperties);
            return response()->json($createdUser, 201);
        } catch (\Throwable $th) {
            //throw $th;
        }

    }

    

    
    
}
