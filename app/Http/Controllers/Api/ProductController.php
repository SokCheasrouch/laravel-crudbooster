<?php

namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\support\Facades\DB;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')
        ->join('cms_users', 'products.user_id',  '=', 'cms_users.id')
        ->join('cateogories', 'products.category_id', '=', 'cateogories.id')
        ->select('products.id', 'products.name', 'products.image', 'products.description', 'cateogories.name as category_name', 'cms_users.name as user_name')->get();
        return response()->json($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required',
            'image' => 'required',
            'category_id' => 'required',
            'user_id' => 'required',
            'description' => 'required'

        ]);

        if ($validator->fails()) {
            $responseArr['message'] = $validator->messages()->first();
            $statusCode = 400;
            return response()->json($responseArr, $statusCode);
        }

        $product = new Product();
        $product->name = $request->name;
        $product->image = $request->image;
        $product->category_id = $request->category_id;
        $product->user_id = $request->user_id;
        $product->description = $request->description;

        $product->save();
        return response()->json(['message'=> 'create new product']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
