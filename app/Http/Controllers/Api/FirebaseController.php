<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends Controller
{

    public $auth;

    function __construct() {
        $factory = (new Factory)->withServiceAccount(__DIR__.'/service-account-firebase.json');
        $this->auth = $factory->createAuth();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $users = $this->auth->listUsers();
            $userList = array();
            foreach ($users as $user) {
                $userList[] = $user;
            }
            return response()->json(['size'=>sizeof($userList), 'data'=>$userList]);
        } catch (Exception $e) {
            error_log('Some message here. error'.$e->getMessage());
            echo $e->getMessage();
            $statusCode = 404; 
            return response()->json(['message'=>$e->getMessage()], $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
