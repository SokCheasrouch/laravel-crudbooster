<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    public function user() {
        return $this->belongsto('cms_users');
    }

    public function categories() {
        return $this->belongsto("Category");
    }

}
